<?php
/**
 * @package                Joomla.Site
 * @subpackage	Templates.beez_20
 * @copyright        Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license                GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

// check modules
/*
 $showRightColumn = ($this->countModules('position-3') or $this->countModules('position-6') or $this->countModules('position-8'));
 $showbottom = ($this->countModules('position-9') or $this->countModules('position-10') or $this->countModules('position-11'));
 $showleft = ($this->countModules('position-4') or $this->countModules('position-7') or $this->countModules('position-5'));

 if ($showRightColumn==0 and $showleft==0) {
 $showno = 0;
 }


 JHtml::_('behavior.framework', true);

 // get params
 $color              = $this->params->get('templatecolor');
 $logo               = $this->params->get('logo');
 $navposition        = $this->params->get('navposition');
 $app                = JFactory::getApplication();
 $doc				= JFactory::getDocument();
 $templateparams     = $app->getTemplate(true)->params;

 $doc->addScript($this->baseurl.'/templates/'.$this->template.'/javascript/md_stylechanger.js', 'text/javascript', true);

 <div class="logoheader">
 <h1 id="logo">
 <?php if ($logo): ?>
 <img src="<?php echo $this->baseurl ?>/<?php echo htmlspecialchars($logo); ?>" alt="<?php echo htmlspecialchars($templateparams->get('sitetitle'));?>" />
 <?php endif;?>
 <?php if (!$logo ): ?>
 <?php echo htmlspecialchars($templateparams->get('sitetitle'));?>
 <?php endif; ?>
 <span class="header1"> <?php echo htmlspecialchars($templateparams->get('sitedescription'));?> </span>
 </h1>
 </div>
 <!-- end logoheader -->
 <ul class="unstyled">
 <li><a href="#main" class="u2"><?php echo JText::_('TPL_BEEZ2_SKIP_TO_CONTENT'); ?> </a></li>
 <li><a href="#nav" class="u2"><?php echo JText::_('TPL_BEEZ2_JUMP_TO_NAV'); ?> </a></li>
 <?php if($showRightColumn ):?>
 <li><a href="#additional" class="u2"><?php echo JText::_('TPL_BEEZ2_JUMP_TO_INFO'); ?> </a></li>
 <?php endif; ?>
 </ul>
 <h2 class="unseen">
 <?php echo JText::_('TPL_BEEZ2_NAV_VIEW_SEARCH'); ?>
 </h2>
 <h3 class="unseen">
 <?php echo JText::_('TPL_BEEZ2_NAVIGATION'); ?>
 </h3>
 <jdoc:include type="modules" name="position-1" />
 <div id="line">
 <div id="fontsize"></div>
 <h3 class="unseen">
 <?php echo JText::_('TPL_BEEZ2_SEARCH'); ?>
 </h3>
 <jdoc:include type="modules" name="position-0" />
 </div>
 <!-- end line -->

 <?php if ($showbottom) : ?>
 <div id="footer-inner">
 <div id="bottom">
 <div class="box box1">
 <jdoc:include type="modules" name="position-9" style="beezDivision" headerlevel="3" />
 </div>
 <div class="box box2">
 <jdoc:include type="modules" name="position-10" style="beezDivision" headerlevel="3" />
 </div>
 <div class="box box3">
 <jdoc:include type="modules" name="position-11" style="beezDivision" headerlevel="3" />
 </div>
 </div>
 </div>
 <?php endif ; ?>
 <div id="footer-sub">
 <div id="footer">
 <jdoc:include type="modules" name="position-14" />
 </div>
 </div>
 </div>
 <jdoc:include type="modules" name="debug" />

 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="eng">
<head>
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap.min.css" type="text/css" media="screen,projection" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap-responsive.min.css" type="text/css" media="screen,projection" />
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/javascript/jquery.1.7.2-min.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/javascript/bootstrap.min.js"></script>
<jdoc:include type="head" />
</head>
<body style="padding-bottom: 40px; padding-top: 60px;background-image:url('<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/calcio_sfondo.jpg');">
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a href="#" class="brand">12 uomo</a>
				<div class="nav-collapse">
					<jdoc:include type="modules" name="position-2" />
				</div>
				<!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span3 well" id="nav">
				<jdoc:include type="modules" name="position-7" style="beezDivision" headerLevel="3" />
			</div>
			<div id="main" class="span9 hero-unit">
				<jdoc:include type="message" />
				<jdoc:include type="component" />
			</div>
		</div>
		<hr />
		<div id="footer-outer" class="row-fluid">
			<div class="span8">
				<h4>&copy; Andrea Sbarra - 2012</h4>
				<a href="https://twitter.com/andrea_sbarra" class="twitter-follow-button" data-show-count="false" data-lang="it">Segui @andrea_sbarra</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</div>
		</div>
	</div>
</body>
</html>
