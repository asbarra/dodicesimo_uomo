<?php


defined('_JEXEC') or die('Restricted access');

	$width=$params->get( 'width' );
	$rpp=$params->get( 'rpp' );
	$shellbg=$params->get( 'shellbg' );
	$shellcolor=$params->get( 'shellcolor' );
	$tweetbg=$params->get( 'tweetbg' );
	$tweetcolor=$params->get( 'tweetcolor' );
	$tweetlinks=$params->get( 'tweetlinks' );
	$username=$params->get( 'username' );
	
	echo '<script charset="utf-8" src="http://widgets.twimg.com/j/2/widget.js"></script>'
	."<script>"
	."new TWTR.Widget({"
	  ."version: 2,"
	  ."type: 'profile',"
	  ."rpp: ".$rpp.","
	  ."interval: 30000,"
	  ."width: ".$width.","
	  ."height: 300,"
	  ."theme: {"
		."shell: {"
		  ."background: '".$shellbg."',"
		  ."color: '".$shellcolor."'"
		."},"
		."tweets: {"
		  ."background: '".$tweetbg."',"
		  ."color: '".$tweetcolor."',"
		  ."links: '".$tweetlinks."'"
		."}"
	  ."},"
	  ."features: {"
		."scrollbar: false,"
		."loop: false,"
		."live: false,"
		."behavior: 'all'"
	  ."}"
	."}).render().setUser('".$username."').start();"
	."</script>";
	

?>
