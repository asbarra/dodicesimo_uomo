<?php
/**
 * @package                Joomla.Site
 * @subpackage	Templates.beez_20
 * @copyright        Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license                GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/*
 background-image:url('<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/calcio_sfondo.jpg');
 */

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="eng">
<head>
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap.min.css" type="text/css" media="screen,projection" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap-responsive.min.css" type="text/css" media="screen,projection" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/12uomo.css" type="text/css" media="screen,projection" />
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/javascript/jquery.1.7.2-min.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/javascript/bootstrap.min.js"></script>
<jdoc:include type="head" />
</head>
<body style="padding-bottom: 40px; padding-top: 60px;">
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a href="#" class="brand">12 uomo</a>
				<div class="nav-collapse">
					<jdoc:include type="modules" name="position-2" />
				</div>
				<!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div id="main" class="hero-unit">
				<jdoc:include type="message" />
				<jdoc:include type="component" />
			</div>
		</div>
		<hr />
		<div id="footer-outer" class="row">
			<div class="span3">
				<h4>&copy; Andrea Sbarra - 2012</h4>
				<a href="https://twitter.com/andrea_sbarra" class="twitter-follow-button" data-show-count="false" data-lang="it">Segui @andrea_sbarra</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				<a href="https://twitter.com/12uomo" class="twitter-follow-button" data-show-count="false" data-lang="it">Segui @12uomo</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</div>
			<div class="span3" id="nav">
				<jdoc:include type="modules" name="position-7" style="beezDivision" headerLevel="3" />
			</div>
			<div class="span3">
				<ul class="unstyled">
				<li>
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
							  if (d.getElementById(id)) return;
							  js = d.createElement(s); js.id = id;
							  js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1&appId=228126587236614";
							  fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));
					</script>
					<div class="fb-like" data-href="http://dodicesimouomo.altervista.org/index.php" data-send="true" data-width="450" data-show-faces="true"></div>
				</li>
				<li>
					<a href="https://twitter.com/share" class="twitter-share-button" data-via="12uomo" data-lang="it">Tweet</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</li>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>
