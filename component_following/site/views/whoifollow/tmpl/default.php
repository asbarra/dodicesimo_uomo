<?php
/**
 * @version		$Id: default.php 15 2009-11-02 18:37:15Z chdemko $
 * @package		Joomla16.Tutorials
 * @subpackage	Components
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @author		Christophe Demko
 * @link		http://joomlacode.org/gf/project/helloworld_1_6/
 * @license		License GNU General Public License version 2 or later
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$document =& JFactory::getDocument();
$document->addScript(JURI::base() . 'components/com_whoifollow/js/whoifollow.js');
$id = JRequest::getVar('id');
?>
<input type="hidden" id="whoifollowfiled" value="<?php echo $id;?>" />

<h1>Guarda chi seguo su twitter!</h1>
<div class="container-fluid" id="result">
	
</div>
