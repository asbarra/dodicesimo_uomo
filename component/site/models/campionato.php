<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelitem library
jimport('joomla.application.component.modelitem');

/**
 * HelloWorld Model
 */
class HelloWorldModelCampionato extends JModelItem
{
	/**
	 * @var object item
	 */
	protected $item;


	/**
	 * Method to auto-populate the model state.
	 *
	 * This method should only be called once per instantiation and is designed
	 * to be called on the first call to the getState() method unless the model
	 * configuration flag to ignore the request is set.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication();
		// Get the message id
		$id = JRequest::getInt('id');
		$this->setState('message.id', $id);

		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);
		parent::populateState();
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'DodicesimoUomo', $prefix = 'DodicesimoUomoTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}


	public function getGiornateNum(){
		$id = JRequest::getVar('id');
		if($id=="campionato2011"){
			$query = "select distinct(giornata) as giornata, catid as catid from #__helloworld where catid='0'";
		}else{
			$query = "select distinct(giornata) as giornata, catid as catid from #__helloworld where catid='1'";
		}
		$this->_db->setQuery($query);
		$giornate = $this->_db->loadAssocList();
		return $giornate;
	}

	public function getGiornate()
	{
		if (!isset($this->item))
		{
			$id = JRequest::getVar('id');
			if($id=="campionato2011"){
				$giornata = JRequest::getVar('giornata', 1);
				$where = "giornata='".$giornata."' and catid='0'";
			}else{
				$giornata = JRequest::getVar('giornata', 'Girone C 1');
				$where = "giornata='".$giornata."' and catid='1'";
			}
			$id = $this->getState('message.id');
			$this->_db->setQuery($this->_db->getQuery(true)
			->from('#__helloworld as h')
			->select('h.greeting, h.giornata, h.id, h.risultato')
			->where($where));
			if (!$this->item = $this->_db->loadAssocList())
			{
				$this->setError($this->_db->getError());
			}
			else
			{
				// Load the JSON string
				$params = new JRegistry;
				$params->loadJSON($this->item->params);
				$this->item->params = $params;

				// Merge global params with item params
				$params = clone $this->getState('params');
				$params->merge($this->item->params);
				$this->item->params = $params;
			}
		}
		return $this->item;
	}
}
