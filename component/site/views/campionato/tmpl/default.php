<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$document =& JFactory::getDocument();

?>
<div class="row-fluid">
	<div class="row-fluid">
		<h1>Calendario</h1>
	</div>
	<div class="row-fluid">
	<?php
	$array_size = count($this->item);
	for($i = 0; $i < $array_size; $i++){
		if(($this->item[$i]['giornata'] != $this->item[$i-1]['giornata'])){
			?>
		<table class="table table-bordered table-striped">
			<tr>
				<th><?php echo $this->item[$i]['giornata'];?> Giornata</th>
				<th>Risultato</th>
			</tr>
			<?php }?>
			<tr>
				<td><a href="index.php?option=com_helloworld&view=helloworld&id=<?php echo $this->item[$i]['id'];?>"><?php echo $this->item[$i]['greeting'];?> </a></td>
				<td><?php echo $this->item[$i]['risultato']; ?></td>
			</tr>
			<?php } ?>
			<?php if(($this->item[$i]['giornata'] != $this->item[$i-1]['giornata'])){?>
		</table>
		<?php }?>
	</div>
	<div class="pagination pagination-centered">
		<ul>
		<?php
		$array_size_num = count($this->giornate);
		for($j = 0; $j < $array_size_num; $j++){
			?>
			<li><a href="index.php?option=com_helloworld&view=campionato&id=<?php if($this->giornate[$j]['catid']==0){ echo "campionato2011";} else{ echo "Europei";} ?>&giornata=<?php echo $this->giornate[$j]['giornata']; ?>"><?php echo $this->giornate[$j]['giornata']; ?> </a></li>
			<?php
		}?>
		</ul>
	</div>
	<?php ?>
</div>
