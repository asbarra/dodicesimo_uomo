<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$document =& JFactory::getDocument();
$document->addScript(JURI::base() . 'components/com_helloworld/layout/dodicesimo.js');
$document->addScript(JURI::base() . 'components/com_helloworld/layout/jquery.horizontal.scroll.js');
$document->addScript(JURI::base() . 'components/com_helloworld/layout/jcarousellite.js');
$document->addStyleSheet(JURI::base() . 'components/com_helloworld/layout/jquery.horizontal.scroll.css');
$document->addStyleSheet(JURI::base() . 'components/com_helloworld/layout/demo_style.css');
?>
<div class="row-fluid">
	<div class="row-fluid">
		<h1>
			Partita: #<?php echo $this->item->greeting;?>
		</h1>
	</div>
	<div class="row-fluid">
		<?php
			$tags = explode(" ", $this->item->greeting);
			$tag1 = $tags[0]; // piece1
			$tag2 = $tags[1]; // piece2
		?>
		<input type="hidden" id="tag1" value="<?php echo $tag1;?>" /> <input type="hidden" id="tag2" value="<?php echo $tag2;?>" /> <input type="hidden" id="dataInizio"
			value="<?php echo $this->item->data_inizio;?> " /> <input type="hidden" id="dataFine" value="<?php echo $this->item->data_fine;?> " />
	</div>

	<div class="row-fluid">
		<div class="row-fluid">
			<div class="span1">&nbsp;</div>
			<div class="span4">
				<h3>Primo quarto d'ora</h3>
			</div>
			<div class="span1">&nbsp;</div>
		</div>
		<div class="row-fluid">
			<div class="span1">
				<button class="prev_primo btn">&lt;</button>
			</div>
			<div class="span9">
				<div class="carousel_primo">
					<ul class="tweetHorizontalStream_primo">
					</ul>
				</div>
			</div>
			<div class="span2">
				<button class="next_primo btn">&gt;</button><br/><br/>
				<a href="https://twitter.com/intent/tweet?button_hashtag=<?php echo $tag1;?>&text=%2315min" class="twitter-hashtag-button" data-lang="it" data-related="andrea_sbarra">#MilanJuve #15min</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</div>
		</div>
	</div>
	<br/>
	
	<div class="row-fluid">
		<div class="row-fluid">
			<div class="span1">&nbsp;</div>
			<div class="span4">
				<h3>Prima mezz'ora</h3>
			</div>
			<div class="span1">&nbsp;</div>
		</div>
		<div class="row-fluid">
			<div class="span1">
				<button class="prev_secondo btn">&lt;</button>
			</div>
			<div class="span9">
				<div class="carousel_secondo">
					<ul class="tweetHorizontalStream_secondo">
					</ul>
				</div>
			</div>
			<div class="span2">
				<button class="next_secondo btn">&gt;</button><br/><br/>
				<a href="https://twitter.com/intent/tweet?button_hashtag=<?php echo $tag1;?>&text=%2330min" class="twitter-hashtag-button" data-lang="it" data-related="andrea_sbarra">#MilanJuve #30min</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</div>
		</div>
	</div>
	<br/>

	<div class="row-fluid">
		<div class="row-fluid">
			<div class="span1">&nbsp;</div>
			<div class="span4">
				<h3>Fine primo tempo </h3>
			</div>
			<div class="span1">&nbsp;</div>
		</div>
		<div class="row-fluid">
			<div class="span1">
				<button class="prev_terzo btn">&lt;</button>
			</div>
			<div class="span9">
				<div class="carousel_terzo">
					<ul class="tweetHorizontalStream_terzo">
					</ul>
				</div>
			</div>
			<div class="span2">
				<button class="next_terzo btn">&gt;</button><br/><br/>
				<a href="https://twitter.com/intent/tweet?button_hashtag=<?php echo $tag1;?>&text=%2345min" class="twitter-hashtag-button" data-lang="it" data-related="andrea_sbarra">#MilanJuve #45min</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</div>
		</div>
	</div>
	<br/>

	<div class="row-fluid">
		<div class="row-fluid">
			<div class="span1">&nbsp;</div>
			<div class="span4">
				<h3>Inizio secondo tempo </h3>
			</div>
			<div class="span1">&nbsp;</div>
		</div>
		<div class="row-fluid">
			<div class="span1">
				<button class="prev_quarto btn">&lt;</button>
			</div>
			<div class="span9">
				<div class="carousel_quarto">
					<ul class="tweetHorizontalStream_quarto">
					</ul>
				</div>
			</div>
			<div class="span2">
				<button class="next_quarto btn">&gt;</button><br/><br/>
				<a href="https://twitter.com/intent/tweet?button_hashtag=<?php echo $tag1;?>&text=%2360min" class="twitter-hashtag-button" data-lang="it" data-related="andrea_sbarra">#MilanJuve #60min</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</div>
		</div>
	</div>
	<br/>

	<div class="row-fluid">
		<div class="row-fluid">
			<div class="span1">&nbsp;</div>
			<div class="span4">
				<h3>Mezz'ora secondo tempo </h3>
			</div>
			<div class="span1">&nbsp;</div>
		</div>
		<div class="row-fluid">
			<div class="span1">
				<button class="prev_quinto btn">&lt;</button>
			</div>
			<div class="span9">
				<div class="carousel_quinto">
					<ul class="tweetHorizontalStream_quinto">
					</ul>
				</div>
			</div>
			<div class="span2">
				<button class="next_quinto btn">&gt;</button><br/><br/>
				<a href="https://twitter.com/intent/tweet?button_hashtag=<?php echo $tag1;?>&text=%2375min" class="twitter-hashtag-button" data-lang="it" data-related="andrea_sbarra">#MilanJuve #75min</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</div>
		</div>
	</div>
	<br/>

	<div class="row-fluid">
		<div class="row-fluid">
			<div class="span1">&nbsp;</div>
			<div class="span4">
				<h3>Ultimo quarto d'ora </h3>
			</div>
			<div class="span1">&nbsp;</div>
		</div>
		<div class="row-fluid">
			<div class="span1">
				<button class="prev_sesto btn">&lt;</button>
			</div>
			<div class="span9">
				<div class="carousel_sesto">
					<ul class="tweetHorizontalStream_sesto">
					</ul>
				</div>
			</div>
			<div class="span2">
				<button class="next_sesto btn">&gt;</button><br/><br/>
				<a href="https://twitter.com/intent/tweet?button_hashtag=<?php echo $tag1;?>&text=%2390min" class="twitter-hashtag-button" data-lang="it" data-related="andrea_sbarra">#MilanJuve #90min</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</div>
		</div>
	</div>
	<br/>
	
	<div class="row-fluid">
		<div class="row-fluid">
			<div class="span1">&nbsp;</div>
			<div class="span4">
				<h3>Le foto dalla partita!</h3>
			</div>
			<div class="span1">&nbsp;</div>
		</div>
		<div class="row-fluid">
			<div class="span1">
				<button class="prev btn">&lt;</button>
			</div>
			<div class="span6">
				<div class="carousel">
					<ul id="ulAppend">
					</ul>
				</div>
			</div>
			<div class="span1">
				<button class="next btn">&gt;</button>
			</div>
		</div>
	</div>
	<br/>
	
	<div class="row-fluid">
		<div class="row-fluid">
			<div class="span1">&nbsp;</div>
			<div class="span4">
				<h3>Il tweet pi&#249; retweettato!</h3>
			</div>
			<div class="span1">&nbsp;</div>
		</div>
		<div class="row-fluid">
			<div class="span1">&nbsp;</div>
			<div class="span4" id="topTweet">
			</div>
			<div class="span1">&nbsp;</div>
		</div>
	</div>
</div>
