$(document).ready(function() {
	
	var tag = $("#tag1").val()+"+OR+"+$("#tag2").val().replace("#", "%23");
	var initTime = Math.round((new Date($("#dataInizio").val().replace("-", "/").replace("-", "/"))).getTime() / 1000);
	var finishTime = Math.round((new Date($("#dataFine").val().replace("-", "/").replace("-", "/"))).getTime() / 1000);	
	var countImage = 0;
	var maxTrackBack = 0;
	var widthUl = 0;
	
	function truncate(_value)
	{
	  if (_value<0) {
		  return "prepartita";
	  }
	  else{
		  var ret = Math.floor(_value);
		  if(ret>=45 && ret <=60)
		  	return "intervallo";
		  if(ret>60){
			  ret = ret -15;
		  }
		  return ret;
	  }
	}
	
	$.getJSON('http://otter.topsy.com/search.json?q=%23'+tag+'&page=1&perpage=100&mintime='+initTime+'&maxtime='+finishTime+'&order=date&callback=?', function(data) {
	/*
	var initTime = Math.round((new Date("2012-02-25 20:45")).getTime() / 1000);
	var finishTime = Math.round((new Date("2012-02-25 22:30")).getTime() / 1000);
	$.getJSON('http://otter.topsy.com/search.json?q=%23MilanJuve&type=tweet&page=1&perpage=100&mintime='+initTime+'&maxtime='+finishTime+'&order=date&callback=?', function(data) {
	 */
		var items = [];
		$.each(data, function(key, val) {
			if(val.list!=undefined){
				$.each(val.list, function(key1, val1) {
					ttime = ((val1.firstpost_date)-(initTime));
					ttime = (ttime*1)/60;
					/*
					console.log(ttime);
					console.log(val1.content);
					*/
					idStr = val1.url.split("/");
					
					if(val1.mytype=="image"){
						var tweetImage = "<li><img src='"+val1.small_thumbnail+"' width='150' height='150'/></li>";
						$("#ulAppend").append(tweetImage);
						countImage = countImage+1;
						console.log(countImage);
					}
					
					if(idStr[5]!=undefined){
						if(val1.content.indexOf("yellow card")!= -1 || val1.content.indexOf("cartellino giallo")!= -1 || val1.content.indexOf("ammoni")!= -1){
							var tweet = "<li style='background-color:yellow;' ><table style='width:350px' class='table'>"
								+"<tr ><td><a href='"+val1.trackback_author_url+"' target='_blank'><img src='"+val1.topsy_author_img+"' /></a><a href='"+val1.trackback_author_url+"' target='_blank'>"+val1.trackback_author_nick+"</a></td>"+
								"<td><span style='float:right'>"+truncate(ttime)+"</span></td></tr>"
								+"<tr style='height:71px;'><td colspan='2'> "+val1.content+"</td></tr>"
								+"<tr><td colspan='2'><a href='"+val1.url+"' target='_blank'><img src='https://si0.twimg.com/images/dev/cms/intents/bird/bird_gray/bird_16_gray.png'/> vedi tweet</a>" +
								" <a href='https://twitter.com/intent/tweet?in_reply_to="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/reply.png'/>rispondi</a> <a href='https://twitter.com/intent/retweet?tweet_id="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/retweet.png'/>retweet</a> <a href='https://twitter.com/intent/favorite?tweet_id="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/favorite.png'/>favorite</a> </div></td></tr></table></li>";
									
						}else if(val1.content.indexOf("red card")!= -1 || val1.content.indexOf("cartellino rosso")!= -1 || val1.content.indexOf("espuls")!= -1){
							var tweet = "<li style='background-color:red;' ><table style='width:350px' class='table'>"
								+"<tr ><td><a href='"+val1.trackback_author_url+"' target='_blank'><img src='"+val1.topsy_author_img+"' /></a><a href='"+val1.trackback_author_url+"' target='_blank'>"+val1.trackback_author_nick+"</a></td>"+
								"<td><span style='float:right'>"+truncate(ttime)+"</span></td></tr>"
								+"<tr style='height:71px;'><td colspan='2'> "+val1.content+"</td></tr>"
								+"<tr><td colspan='2'><a href='"+val1.url+"' target='_blank'><img src='https://si0.twimg.com/images/dev/cms/intents/bird/bird_gray/bird_16_gray.png'/> vedi tweet</a>" +
								" <a href='https://twitter.com/intent/tweet?in_reply_to="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/reply.png'/>rispondi</a> <a href='https://twitter.com/intent/retweet?tweet_id="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/retweet.png'/>retweet</a> <a href='https://twitter.com/intent/favorite?tweet_id="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/favorite.png'/>favorite</a> </div></td></tr></table></li>";
								
						}else{
							var tweet = "<li><table style='width:350px' class='table'>"
								+"<tr ><td><a href='"+val1.trackback_author_url+"' target='_blank'><img src='"+val1.topsy_author_img+"' /></a><a href='"+val1.trackback_author_url+"' target='_blank'>"+val1.trackback_author_nick+"</a></td>"+
								"<td><span style='float:right'>"+truncate(ttime)+"</span></td></tr>"
								+"<tr style='height:71px;'><td colspan='2'> "+val1.content+"</td></tr>"
								+"<tr><td colspan='2'><a href='"+val1.url+"' target='_blank'><img src='https://si0.twimg.com/images/dev/cms/intents/bird/bird_gray/bird_16_gray.png'/> vedi tweet</a>" +
								" <a href='https://twitter.com/intent/tweet?in_reply_to="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/reply.png'/>rispondi</a> <a href='https://twitter.com/intent/retweet?tweet_id="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/retweet.png'/>retweet</a> <a href='https://twitter.com/intent/favorite?tweet_id="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/favorite.png'/>favorite</a> </div></td></tr></table></li>";
						}
						
						if(ttime>0 && ttime <= 15){
							$(".tweetHorizontalStream_primo").prepend(tweet);
						}
						else if(ttime>15 && ttime <= 30){
							$(".tweetHorizontalStream_secondo").prepend(tweet);
						}
						else if(ttime>30 && ttime <= 45){
							$(".tweetHorizontalStream_terzo").prepend(tweet);
						}
						else if(ttime>60 && ttime <= 75){
							$(".tweetHorizontalStream_quarto").prepend(tweet);
						}
						else if(ttime>75 && ttime <= 90){
							$(".tweetHorizontalStream_quinto").prepend(tweet);
						}
						else if(ttime>90 && ttime <= 105){
							$(".tweetHorizontalStream_sesto").prepend(tweet);
						}
						
						if(val1.trackback_total > maxTrackBack){
							maxTrackBack = val1.trackback_total;
							$("#topTweet").html(tweet);
						}
					}
				});
			}
		});
		
		$(".carousel_primo").jCarouselLite({
			btnNext: ".next_primo",
		    btnPrev: ".prev_primo",
		    visible:2
		});
		
		$(".carousel_secondo").jCarouselLite({
			btnNext: ".next_secondo",
		    btnPrev: ".prev_secondo",
		    visible:2
		});
		
		$(".carousel_terzo").jCarouselLite({
			btnNext: ".next_terzo",
		    btnPrev: ".prev_terzo",
		    visible:2
		});
		
		$(".carousel_quarto").jCarouselLite({
			btnNext: ".next_quarto",
		    btnPrev: ".prev_quarto",
		    visible:2
		});
		
		$(".carousel_quinto").jCarouselLite({
			btnNext: ".next_quinto",
		    btnPrev: ".prev_quinto",
		    visible:2
		});
		
		$(".carousel_sesto").jCarouselLite({
			btnNext: ".next_sesto",
		    btnPrev: ".prev_sesto",
		    visible:2
		});
		
		$(".carousel").jCarouselLite({
			btnNext: ".next",
		    btnPrev: ".prev",
		    visible:2
		});
		
	});
	
	
	var quarter = $("#tag1").val()+"+AND+%2315min";
	$.getJSON('http://otter.topsy.com/search.json?q=%23'+quarter+'&page=1&perpage=100&mintime='+finishTime+'&order=date&callback=?', function(data) {
		var items = [];
		$.each(data, function(key, val) {
			if(val.list!=undefined){
				$.each(val.list, function(key1, val1) {
					idStr = val1.url.split("/");
					
					if(idStr[5]!=undefined){
						if(val1.content.indexOf("yellow card")!= -1 || val1.content.indexOf("cartellino giallo")!= -1 || val1.content.indexOf("ammoni")!= -1){
							var tweet = "<li style='background-color:yellow;' ><table style='width:350px' class='table'>"
								+"<tr ><td><a href='"+val1.trackback_author_url+"' target='_blank'><img src='"+val1.topsy_author_img+"' /></a><a href='"+val1.trackback_author_url+"' target='_blank'>"+val1.trackback_author_nick+"</a></td>"+
								"<td><span style='float:right'>15min</span></td></tr>"
								+"<tr style='height:71px;'><td colspan='2'> "+val1.content+"</td></tr>"
								+"<tr><td colspan='2'><a href='"+val1.url+"' target='_blank'><img src='https://si0.twimg.com/images/dev/cms/intents/bird/bird_gray/bird_16_gray.png'/> vedi tweet</a>" +
								" <a href='https://twitter.com/intent/tweet?in_reply_to="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/reply.png'/>rispondi</a> <a href='https://twitter.com/intent/retweet?tweet_id="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/retweet.png'/>retweet</a> <a href='https://twitter.com/intent/favorite?tweet_id="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/favorite.png'/>favorite</a> </div></td></tr></table></li>";
									
						}else if(val1.content.indexOf("red card")!= -1 || val1.content.indexOf("cartellino rosso")!= -1 || val1.content.indexOf("espuls")!= -1){
							var tweet = "<li style='background-color:red;' ><table style='width:350px' class='table'>"
								+"<tr ><td><a href='"+val1.trackback_author_url+"' target='_blank'><img src='"+val1.topsy_author_img+"' /></a><a href='"+val1.trackback_author_url+"' target='_blank'>"+val1.trackback_author_nick+"</a></td>"+
								"<td><span style='float:right'>15min</span></td></tr>"
								+"<tr style='height:71px;'><td colspan='2'> "+val1.content+"</td></tr>"
								+"<tr><td colspan='2'><a href='"+val1.url+"' target='_blank'><img src='https://si0.twimg.com/images/dev/cms/intents/bird/bird_gray/bird_16_gray.png'/> vedi tweet</a>" +
								" <a href='https://twitter.com/intent/tweet?in_reply_to="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/reply.png'/>rispondi</a> <a href='https://twitter.com/intent/retweet?tweet_id="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/retweet.png'/>retweet</a> <a href='https://twitter.com/intent/favorite?tweet_id="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/favorite.png'/>favorite</a> </div></td></tr></table></li>";
								
						}else{
							var tweet = "<li><table style='width:350px' class='table'>"
								+"<tr ><td><a href='"+val1.trackback_author_url+"' target='_blank'><img src='"+val1.topsy_author_img+"' /></a><a href='"+val1.trackback_author_url+"' target='_blank'>"+val1.trackback_author_nick+"</a></td>"+
								"<td><span style='float:right'>15min</span></td></tr>"
								+"<tr style='height:71px;'><td colspan='2'> "+val1.content+"</td></tr>"
								+"<tr><td colspan='2'><a href='"+val1.url+"' target='_blank'><img src='https://si0.twimg.com/images/dev/cms/intents/bird/bird_gray/bird_16_gray.png'/> vedi tweet</a>" +
								" <a href='https://twitter.com/intent/tweet?in_reply_to="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/reply.png'/>rispondi</a> <a href='https://twitter.com/intent/retweet?tweet_id="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/retweet.png'/>retweet</a> <a href='https://twitter.com/intent/favorite?tweet_id="+idStr[5]+"'><img src='https://si0.twimg.com/images/dev/cms/intents/icons/favorite.png'/>favorite</a> </div></td></tr></table></li>";
						}
						$(".tweetHorizontalStream_primo").append(tweet);
					}
				});
			}
			});
		
		$(".carousel_primo").jCarouselLite({
			btnNext: ".next_primo",
		    btnPrev: ".prev_primo",
		    visible:2
		});
	});
	
	
	$("#tweetButtonHere").html('<a href="https://twitter.com/intent/tweet?button_hashtag='+$("#tag1").val()+'" class="twitter-hashtag-button" data-lang="it" data-related="12Uomo">Tweet #'+$("#tag1").val()+'</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>');
	
});