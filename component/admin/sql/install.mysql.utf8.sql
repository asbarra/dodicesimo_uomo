DROP TABLE IF EXISTS `#__helloworld`;
 

CREATE TABLE `#__helloworld` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `greeting` varchar(25) NOT NULL,
  `catid` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `data_inizio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_fine` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `giornata` varchar(25) NOT NULL,
  `risultato` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `#__helloworld` VALUES ('1', 'SienaFiorentina', '0', '', '2011-12-20 18:00:00', '2011-12-20 19:45:00', '1', '0 - 0');
INSERT INTO `#__helloworld` VALUES ('2', 'CagliariMilan', '0', '', '2011-12-20 20:45:00', '2011-12-20 22:30:00', '1', '0 - 2');
INSERT INTO `#__helloworld` VALUES ('3', 'UdineseJuve', '0', '', '2011-12-21 18:00:00', '2011-12-21 19:45:00', '1', '0 - 0');
INSERT INTO `#__helloworld` VALUES ('4', 'AtalantaCesena', '0', '', '2011-12-21 20:45:00', '2011-12-21 22:30:00', '1', '4 - 1');
INSERT INTO `#__helloworld` VALUES ('5', 'BolognaRoma', '0', '', '2011-12-21 20:45:00', '2011-12-21 22:30:00', '1', '0 - 2');
INSERT INTO `#__helloworld` VALUES ('6', 'InterLecce', '0', '', '2011-12-21 20:45:00', '2011-12-21 22:30:00', '1', '4 - 1');
INSERT INTO `#__helloworld` VALUES ('7', 'LazioChievo', '0', '', '2011-12-21 20:45:00', '2011-12-21 22:30:00', '1', '0 - 0');
INSERT INTO `#__helloworld` VALUES ('8', 'NapoliGenoa', '0', '', '2011-12-21 20:45:00', '2011-12-21 22:30:00', '1', '6 - 1');
INSERT INTO `#__helloworld` VALUES ('9', 'NovaraPalermo', '0', '', '2011-12-21 20:45:00', '2011-12-21 22:30:00', '1', '2 - 2');
INSERT INTO `#__helloworld` VALUES ('10', 'ParmaCatania', '0', '', '2011-12-21 20:45:00', '2011-12-21 22:30:00', '1', '3 - 3');

INSERT INTO `#__helloworld` VALUES ('11', 'MilanLazio', '0', '', '2011-09-09 20:45:00', '2011-09-09 22:45:00', '2', '2 - 2');
INSERT INTO `#__helloworld` VALUES ('12', 'CesenaNapoli', '0', '', '2011-09-10 20:45:00', '2011-09-10 22:30:00', '2', '1 - 3');
INSERT INTO `#__helloworld` VALUES ('13', 'JuveParma', '0', '', '2011-09-11 12:30:00', '2011-09-11 14:15:00', '2', '4 - 1');
INSERT INTO `#__helloworld` VALUES ('14', 'CataniaSiena', '0', '', '2011-09-11 15:00:00', '2011-09-11 16:45:00', '2', '0 - 0');
INSERT INTO `#__helloworld` VALUES ('15', 'ChievoNovara', '0', '', '2011-09-11 15:00:00', '2011-09-11 16:45:00', '2', '2 - 2');
INSERT INTO `#__helloworld` VALUES ('16', 'FiorentinaBologna', '0', '', '2011-09-11 15:00:00', '2011-09-11 16:45:00', '2', '2 - 0');
INSERT INTO `#__helloworld` VALUES ('17', 'GenoaAtalanta', '0', '', '2011-09-11 15:00:00', '2011-09-11 16:45:00', '2', '2 - 2');
INSERT INTO `#__helloworld` VALUES ('18', 'LecceUdinese', '0', '', '2011-09-11 15:00:00', '2011-09-11 16:45:00', '2', '0 - 2');
INSERT INTO `#__helloworld` VALUES ('19', 'RomaCagliari', '0', '', '2011-09-11 15:00:00', '2011-09-11 16:45:00', '2', '1 - 2');
INSERT INTO `#__helloworld` VALUES ('20', 'PalermoInter', '0', '', '2011-09-11 20:45:00', '2011-09-11 22:30:00', '2', '4 - 3');

INSERT INTO `#__helloworld` VALUES ('21', 'CagliariNovara', '0', '', '2011-09-17 18:00:00', '2011-09-17 19:45:00', '3', '2 - 1');
INSERT INTO `#__helloworld` VALUES ('22', 'InterRoma', '0', '', '2011-09-17 20:45:00', '2011-09-17 22:30:00', '3', '0 - 0');
INSERT INTO `#__helloworld` VALUES ('23', 'AtalantaPalermo', '0', '', '2011-09-18 12:30:00', '2011-09-18 14:15:00', '3', '1 - 0');
INSERT INTO `#__helloworld` VALUES ('24', 'BolognaLecce', '0', '', '2011-09-18 15:00:00', '2011-09-18 16:45:00', '3', '0 - 2');
INSERT INTO `#__helloworld` VALUES ('25', 'CataniaCesena', '0', '', '2011-09-18 15:00:00', '2011-09-18 16:45:00', '3', '1 - 0');
INSERT INTO `#__helloworld` VALUES ('26', 'LazioGenoa', '0', '', '2011-09-18 15:00:00', '2011-09-18 16:45:00', '3', '1 - 2');
INSERT INTO `#__helloworld` VALUES ('27', 'SienaJuventus', '0', '', '2011-09-18 15:00:00', '2011-09-18 16:45:00', '3', '0 - 1');
INSERT INTO `#__helloworld` VALUES ('28', 'ParmaChievo', '0', '', '2011-09-18 15:00:00', '2011-09-18 16:45:00', '3', '2 - 1');
INSERT INTO `#__helloworld` VALUES ('29', 'UdineseFiorentina', '0', '', '2011-09-18 15:00:00', '2011-09-18 16:45:00', '3', '2 - 0');
INSERT INTO `#__helloworld` VALUES ('30', 'NapoliMilan', '0', '', '2011-09-18 20:45:00', '2011-09-18 22:30:00', '3', '3 - 1');

INSERT INTO `#__helloworld` VALUES ('31', 'NovaraInter', '0', '', '2011-09-20 20:45:00', '2011-09-20 22:30:00', '4', '3 - 1');
INSERT INTO `#__helloworld` VALUES ('32', 'CesenaLazio', '0', '', '2011-09-21 20:45:00', '2011-09-21 22:30:00', '4', '1 - 2');
INSERT INTO `#__helloworld` VALUES ('33', 'ChievoNapoli', '0', '', '2011-09-21 20:45:00', '2011-09-11 22:30:00', '4', '1 - 0');
INSERT INTO `#__helloworld` VALUES ('34', 'FiorentinaParma', '0', '', '2011-09-21 20:45:00', '2011-09-21 22:30:00', '4', '3 - 0');
INSERT INTO `#__helloworld` VALUES ('35', 'GenoaCatania', '0', '', '2011-09-21 20:45:00', '2011-09-21 22:30:00', '4', '3 - 0');
INSERT INTO `#__helloworld` VALUES ('36', 'JuveBologna', '0', '', '2011-09-21 20:45:00', '2011-09-21 22:30:00', '4', '1 - 1');
INSERT INTO `#__helloworld` VALUES ('37', 'LecceAtalanta', '0', '', '2011-09-21 20:45:00', '2011-09-21 22:30:00', '4', '1 - 2');
INSERT INTO `#__helloworld` VALUES ('38', 'MilanUdinese', '0', '', '2011-09-21 20:45:00', '2011-09-21 22:30:00', '4', '1 - 1');
INSERT INTO `#__helloworld` VALUES ('39', 'PalermoCagliari', '0', '', '2011-09-21 20:45:00', '2011-09-21 22:30:00', '4', '3 - 2');
INSERT INTO `#__helloworld` VALUES ('40', 'RomaSiena', '0', '', '2011-09-22 20:45:00', '2011-09-22 22:30:00', '4', '1 - 1');

INSERT INTO `#__helloworld` VALUES ('41', 'BolognaInter', '0', '', '2011-09-24 18:00:00', '2011-09-24 19:45:00', '5', '1 - 3');
INSERT INTO `#__helloworld` VALUES ('42', 'MilanCesena', '0', '', '2011-09-24 20:45:00', '2011-09-24 22:30:00', '5', '1 - 0');
INSERT INTO `#__helloworld` VALUES ('43', 'NapoliFiorentina', '0', '', '2011-09-24 20:45:00', '2011-09-24 22:30:00', '5', '0 - 0');
INSERT INTO `#__helloworld` VALUES ('44', 'ChievoGenoa', '0', '', '2011-09-25 12:30:00', '2011-09-25 14:15:00', '5', '2 - 1');
INSERT INTO `#__helloworld` VALUES ('45', 'AtalantaNovara', '0', '', '2011-09-25 15:00:00', '2011-09-25 16:45:00', '5', '2 - 1');
INSERT INTO `#__helloworld` VALUES ('46', 'CagliariUdinese', '0', '', '2011-09-25 15:00:00', '2011-09-25 16:45:00', '5', '0 - 0');
INSERT INTO `#__helloworld` VALUES ('47', 'CataniaJuventus', '0', '', '2011-09-25 15:00:00', '2011-09-25 16:45:00', '5', '1 - 1');
INSERT INTO `#__helloworld` VALUES ('48', 'LazioPalermo', '0', '', '2011-09-25 15:00:00', '2011-09-25 16:45:00', '5', '0 - 0');
INSERT INTO `#__helloworld` VALUES ('49', 'SienaLecce', '0', '', '2011-09-25 15:00:00', '2011-09-25 16:45:00', '5', '3 - 0');
INSERT INTO `#__helloworld` VALUES ('50', 'ParmaRoma', '0', '', '2011-09-25 20:45:00', '2011-09-25 22:30:00', '5', '0 - 1');


INSERT INTO `#__helloworld` VALUES ('60', 'MilanJuve #MilJuv', '0', '', '2012-02-25 20:45:00', '2012-02-25 22:30:00', '25', '1 - 1');

INSERT INTO `#__helloworld` VALUES ('61', 'SpagnaItalia #SpaIta', '1', '', '2012-06-10 18:00:00', '2012-06-10 19:30:00', 'Girone C 1', '1 - 1');
INSERT INTO `#__helloworld` VALUES ('62', 'ItaliaCroazia #ItaCro', '1', '', '2012-06-14 18:00:00', '2012-06-14 19:30:00', 'Girone C 2', '1 - 1');
INSERT INTO `#__helloworld` VALUES ('63', 'ItaliaIrlanda #ItaIre', '1', '', '2012-06-18 20:45:00', '2012-06-18 22:30:00', 'Girone C 3', '2 - 0');
INSERT INTO `#__helloworld` VALUES ('64', 'InghilterraItalia #EngIta', '1', '', '2012-06-25 20:45:00', '2012-06-25 22:30:00', 'Quarti', '2 - 4');
INSERT INTO `#__helloworld` VALUES ('65', 'GermaniaItalia #GerIta', '1', '', '2012-06-25 20:45:00', '2012-06-25 22:30:00', 'Semifinale', '1 - 2');
INSERT INTO `#__helloworld` VALUES ('66', 'SpagnaItalia #SpaIta', '1', '', '2012-07-01 20:45:00', '2012-07-01 22:30:00', 'Finale', '0 - 0');



